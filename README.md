# PTBP Documentation

## Gitlab Pages Website
The Pages are desigened to store the Slater Koster files generated from [PTBP][1]
[mbook](https://mncui.gitlab.io/mbook)

### PTBP_Lab
The performance of PTBP set for each element

### PTBPlus
The introduction of multi-fidelity learning 
- [ ] todo


[1]: https://doi.org/10.1021/acs.jctc.4c00228 "J. Chem. Theory Comput. 2024, 20, 12, 5276–5290"

