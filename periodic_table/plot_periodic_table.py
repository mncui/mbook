from bokeh.plotting import figure, show
from bokeh.sampledata.periodic_table import elements
from bokeh.transform import dodge, factor_cmap
import copy
from bokeh.plotting import figure, output_file, show, ColumnDataSource
from bokeh.models import HoverTool
import matplotlib.colors as colors
import pandas as pd


df = pd.read_excel('hot_bbi_opt.xlsx')

periods = ["I", "II", "III", "IV", "V", "VI", "VII", "LA", "AC"]
groups = [str(x) for x in range(1, 19)]
output_file("toolbar.html")

# df = elements.copy()
# df["atomic mass"] = df["atomic mass"].astype(str)
# df["group"] = df["group"].astype(str)
# df["period"] = [periods[x-1] for x in df.period]
df["atomic radius"] = df["atomic radius"] /2 
df = df[df.group != "-"]
df = df[df.symbol != "Lr"]
# df = df[df.symbol != "Lu"]

cmap = {
    "<3%"               : "#1f640a", # green
    "<5%"               : "#2694ab", # deepblue
    "<10%"              : "#6b48ff", # purple
    "Noble gas"              : "#84929e",

}

hover = HoverTool(
        tooltips="""
        <div>
            <div>
                <img
                    src="@imgs3" height="450" alt="@name" width="525"
                    style="float: right; margin: 300px 0px 0px 0px;"
                    border="0"
                    align="right"
                ></img>
            </div>
            <div>
                <span style="font-size: 17px; font-weight: bold;">@name:</span>
            </div>

        </div>
        """
    )

p = figure(title="Periodic Table", width=1000, height=600,
           x_range=groups, y_range=list(reversed(periods)),toolbar_location="below",
           tools=[hover])
        #    tools="hover", toolbar_location=None, tooltips=TOOLTIPS)

# r = p.rect("group", "period", 0.95, 0.95, source=df, fill_alpha=0.6, legend_field="metal",
#            color=factor_cmap('metal', palette=list(cmap.values()), factors=list(cmap.keys())))
r = p.circle("group", "period", size="r_wave_fig", source=df, fill_alpha=0.5, \
           legend_field="Perfermance", color=factor_cmap('Perfermance', palette=list(cmap.values()), factors=list(cmap.keys())))
text_props = dict(source=df, text_align="left", text_baseline="middle")

x = dodge("group", -0.4, range=p.x_range)

p.text(x=x, y="period", text="symbol", text_font_style="bold", **text_props)

p.text(x=x, y=dodge("period", 0.3, range=p.y_range), text="atomic number",
       text_font_size="11px", **text_props)

p.text(x=x, y=dodge("period", -0.35, range=p.y_range), text="name",
       text_font_size="7px", **text_props)

p.text(x=x, y=dodge("period", -0.2, range=p.y_range), text="atomic mass",
       text_font_size="7px", **text_props)

# p.text(x=["3", "3"], y=["VI", "VII"], text=["LA", "AC"], text_align="center", text_baseline="middle")


p.outline_line_color = None
p.grid.grid_line_color = None
p.axis.axis_line_color = None
p.axis.major_tick_line_color = None
p.axis.major_label_standoff = 0
p.legend.orientation = "horizontal"
p.legend.location ="top_center"
# p.hover.renderers = [r] # only hover element boxes

show(p)

# from bokeh.io import export_png
# export_png(p, filename="plot.png", height=500, width=1000)