# PTBPplus

This code is designed for the workflow of Machine Learning based on the [PTBP](https://gitlab.mpcdf.mpg.de/mncui/dftb_parameterization) parameter set.

## 1. MAIN SCRIPTS
### 1. **multi_fidelity.py**
This is the main code for functions like merging dataset, calculating delta values, spliting dataset, doing a quick Machine Learning, ploting, et al.

### 2. **Mtools.py**
The function collection



