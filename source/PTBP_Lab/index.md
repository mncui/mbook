# PTBP_Lab

**Density Functional Tight-Binding Theory Parameterization: Periodic Table Baseline Parameter Set (PTBP)[PTBP][1]**



<div align="center">
  <img src="https://gitlab.com/mncui/ptbp_lab/-/raw/main/utils/ptbp.png?ref_type=heads" alt="PTBP Logo" width="30%"/>
</div>


## Overall Performance of All Elements

- All parameter are optimized based on different configurations that decided by boltzmann factor weights ($p_i$).
$p_i=\frac{e^{-\left(E_i\right) / k_{\mathrm{B}} T}}{\sum_j e^{-\left(E_j\right) / k_{\mathrm{B}} T}}$

    Only count configurations with $p_i>0.1$

- Only energies are used for the fitting of noble gases

- The performance is labeled with different colors ($\mu_V <3\% $: green, $\mu_V < 5\% $: blue, $\mu_V < 10\%$: purple).

<div align="center">
  <iframe src="/_static/periodic_table.html" width="100%" height="800px" style="border:none; overflow:auto;"></iframe>
</div>



---

## References

[1]: https://doi.org/10.1021/acs.jctc.4c00228 "J. Chem. Theory Comput. 2024, 20, 12, 5276–5290"
