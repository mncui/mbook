# SKF File Download

Welcome to the SKF File Download page. You can download the corresponding SKF files by selecting options below.

The ParameterSets folder includes parameter sets PRIOR, PTBP, and QN+Rep, as introduced in the article [PTBP][1]. 

Although these parameter files were not specifically optimized for compounds, they still demonstrate relatively reliable performance, as shown in Part 3.2 of the article [PTBP][1], which benchmarks binary systems (hydrides, nitrides, oxides, and fluorides).

Furthermore, the code for processing the parameterization can be found at [PTBP_lab][3]. We are pleased to share the SKF files for free use and hope they provide convenience to those exploring the DFTB method.


## **Options**

<div id="skf-container">
  
  ### **1. Select Category**

  The full set of these parameters can be downloaded from [ZENODO][2]
  
  Please select a category from the dropdown menu:
  - **PTBP**: The parameter set generated from this work [PTBP][1]
  - **QN_rep**: The electronic parameters $r^0_\mathrm{w}$, $r^0_\mathrm{d}$, and $p$ are obtained from QUASINANO2013 (J. Chem. Theory Comput. 2013, 9, 9, 4006-4017), and the repulsive part is fitted in the same way as PTBP
  
  Note: the large exponent `p` inside the confinement potential seems to cause convergence problems while generating slater-koster files, so it includes all the elemental parameter files and limited heterogeneous files only for the benchmark of binary in [PTBP][1] ($\left( \frac{r}{r_0} \right)^p$)

  - **PRIOR**: The electronic parameters of $r^0_\mathrm{w}$ and $r^0_\mathrm{d}$ are restricted to 2 and 4 times covalent radii $r_\mathrm{cov}$, $p$=2, and the repulsive part is fitted in the same way as PTBP
  
  <select id="category-select" style="padding: 6px; margin-top: 10px; width: 100%;">
    <option value="">-- Select Category --</option>
    <option value="PTBP">PTBP</option>
    <option value="QN_rep">QN_rep</option>
    <option value="PRIOR">PRIOR</option>
  </select>
  
  ### **2. Enter Elements**
  
  Please enter the elements you wish to include (e.g., `Li, Cu, S` or `Li Cu S`). You can separate elements using commas or any number of spaces:

  STO coefficients: [ptbp_wfc.hsd][4]

  <input type="text" id="elements-input" placeholder="Enter element symbols separated by commas or spaces" style="width: 100%; padding: 8px; margin-top: 10px;">
  
  ### **3. Generate and Download**
  
  <button id="generate-download" style="margin-top: 20px; padding: 10px 20px; background-color: #4CAF50; color: white; border: none; border-radius: 4px; cursor: pointer;">Generate and Download SKF Files</button>
  
  <!-- Loading Indicator -->
  <div id="spinner" style="display: none; margin-top: 10px;">
    <p>Processing, please wait...</p>
  </div>
  
  <!-- Download Link Container -->
  <div id="download-link-container" style="margin-top: 20px;"></div>

  <!-- Missing Files Container -->
  <div id="missing-files-container" style="margin-top: 20px; color: red;"></div>

</div>

<!-- Include JSZip library (removed integrity attribute to prevent loading issues) -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.10.1/jszip.min.js" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<!-- Alternative CDN options if needed -->
<!-- <script src="https://unpkg.com/jszip@3.10.1/dist/jszip.min.js" crossorigin="anonymous" referrerpolicy="no-referrer"></script> -->
<!-- <script src="https://cdn.jsdelivr.net/npm/jszip@3.10.1/dist/jszip.min.js" crossorigin="anonymous" referrerpolicy="no-referrer"></script> -->

<script>
  // List of valid chemical element symbols
  const validElements = [
    "H", "He", "Li", "Be", "B", "C", "N", "O", "F", "Ne",
    "Na", "Mg", "Al", "Si", "P", "S", "Cl", "Ar",
    "K", "Ca", "Sc", "Ti", "V", "Cr", "Mn", "Fe", "Co", "Ni", "Cu", "Zn",
    "Ga", "Ge", "As", "Se", "Br", "Kr",
    "Rb", "Sr", "Y", "Zr", "Nb", "Mo", "Tc", "Ru", "Rh", "Pd", "Ag", "Cd",
    "In", "Sn", "Sb", "Te", "I", "Xe",
    "Cs", "Ba", "La", "Lu", "Hf", "Ta", "W", "Re", "Os", "Ir", "Pt", "Au", "Hg",
    "Tl", "Pb", "Bi", "Po", "At", "Rn",
    "Ra", "Th", 
  ];

  document.getElementById('generate-download').addEventListener('click', async () => {
    const category = document.getElementById('category-select').value.trim();
    const elementsInput = document.getElementById('elements-input').value.trim();

    // Clear previous download links and messages
    document.getElementById('download-link-container').innerHTML = '';
    document.getElementById('missing-files-container').innerHTML = '';

    if (!category) {
      alert('Please select a category.');
      return;
    }

    if (!elementsInput) {
      alert('Please enter at least one element.');
      return;
    }

    // Parse elements, split by commas or spaces, and remove empty entries
    const elements = elementsInput.split(/[\s,]+/).filter(el => el);

    if (elements.length === 0) {
      alert('Please enter valid element symbols.');
      return;
    }

    // Validate elements
    const invalidElements = elements.filter(el => !validElements.includes(el));
    if (invalidElements.length > 0) {
      alert(`Invalid element symbols: ${invalidElements.join(', ')}`);
      return;
    }

    // Show loading indicator
    document.getElementById('spinner').style.display = 'block';

    // Generate all possible two-element combinations (including same elements)
    let fileNames = [];
    elements.forEach(el1 => {
      elements.forEach(el2 => {
        const fileName = `${el1}-${el2}.skf`;
        fileNames.push(fileName);
      });
    });

    // Optionally remove duplicate combinations (e.g., Cu-S and S-Cu)
    // If you want to keep all combinations, comment out the following line
    // fileNames = fileNames.filter((file, index, self) => self.indexOf(file) === index);

    // Create ZIP file
    const zip = new JSZip();
    const folder = zip.folder(`${category}_skfs`);

    // Arrays to keep track of missing files
    const missingFiles = [];

    // Function to check if a file exists
    async function fileExists(url) {
      try {
        const response = await fetch(url, { method: 'HEAD' });
        return response.ok;
      } catch (error) {
        console.error(`Error checking file existence: ${url}`, error);
        return false;
      }
    }

    // Download and add existing files to ZIP
    for (const fileName of fileNames) {
      // const filePath = `/../source/_static/${category.toLowerCase()}_skfs/${fileName}`; // Corrected file path
      const filePath = `https://ptbp.duckdns.org/${category.toLowerCase()}_skfs/${fileName}`;
      const exists = await fileExists(filePath);
      if (exists) {
        try {
          const response = await fetch(filePath);
          const blob = await response.blob();
          folder.file(fileName, blob);
        } catch (error) {
          console.error(`Error downloading file: ${fileName}`, error);
        }
      } else {
        console.warn(`File not found: ${fileName}`);
        missingFiles.push(fileName);
      }
    }

    // Generate ZIP and provide download link
    try {
      const content = await zip.generateAsync({ type: 'blob' });
      const zipURL = URL.createObjectURL(content);
      const downloadLink = document.createElement('a');
      downloadLink.href = zipURL;
      downloadLink.download = `${category}_skfs.zip`;
      downloadLink.textContent = `Download ${category}_skfs.zip`;
      downloadLink.style.display = 'inline-block';
      downloadLink.style.padding = '10px 20px';
      downloadLink.style.backgroundColor = '#4CAF50';
      downloadLink.style.color = 'white';
      downloadLink.style.border = 'none';
      downloadLink.style.borderRadius = '4px';
      downloadLink.style.textDecoration = 'none';
      downloadLink.style.cursor = 'pointer';
      
      document.getElementById('download-link-container').appendChild(downloadLink);

      // If there are missing files, display a warning message
      if (missingFiles.length > 0) {
        const warningPara = document.createElement('p');
        warningPara.textContent = `The following files were not found and are not included in the ZIP: ${missingFiles.join(', ')}`;
        document.getElementById('missing-files-container').appendChild(warningPara);
      }

    } catch (error) {
      console.error('Error generating ZIP file:', error);
      alert('An error occurred while generating the ZIP file. Please try again later.');
    }

    // Hide loading indicator
    document.getElementById('spinner').style.display = 'none';
  });
</script>

<style>
  /* Ensure styles only affect elements within #skf-container to avoid conflicts */
  #skf-container {
    max-width: 600px; /* Set maximum width as needed */
    margin: 0 auto; /* Center the container */
    text-align: left; /* Align text to the left */
  }

  /* Basic styles for select and input elements */
  #category-select, #elements-input {
    box-sizing: border-box;
    width: 100%;
    padding: 8px;
    margin-top: 10px;
  }

  /* Hover effect for the generate-download button */
  #generate-download:hover {
    background-color: #45a049;
  }

  /* Hover effect for the download link */
  #download-link-container a:hover {
    background-color: #45a049;
  }

  /* Loading indicator styles */
  #spinner p {
    text-align: center;
    font-size: 16px;
    color: #555;
  }

  /* Download link button styles */
  #download-link-container a {
    display: inline-block;
    padding: 10px 20px;
    background-color: #4CAF50;
    color: white;
    border: none;
    border-radius: 4px;
    text-decoration: none;
    cursor: pointer;
  }

  /* Warning message styles */
  #missing-files-container p {
    background-color: #ffe6e6;
    padding: 10px;
    border-left: 6px solid #f44336;
    margin: 10px 0 0 0;
    border-radius: 4px;
  }
</style>



[1]: https://doi.org/10.1021/acs.jctc.4c00228 "J. Chem. Theory Comput. 2024, 20, 12, 5276–5290"
[2]: https://doi.org/10.5281/zenodo.10677795 "Parameter sets"
[3]: https://gitlab.com/mncui/ptbp_lab "PTBP_lab"
[4]: https://ptbp.duckdns.org/ptbp_wfc.hsd "STO coefficients"
